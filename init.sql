CREATE TABLE wikimap (
  map_wiki SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  map_wikiname VARBINARY(32) NOT NULL UNIQUE -- to match gil_wiki
);

CREATE TABLE dumps (
  dumpid   TINYINT PRIMARY KEY, -- index into fil_dumpused
  dumpname CHAR(8) NOT NULL UNIQUE -- YYYYMMDD date stamp of dump
);

CREATE TABLE formerimagelinks (
  fil_wiki     SMALLINT UNSIGNED NOT NULL, -- references wikimap
  fil_frompage INT(10)  UNSIGNED NOT NULL, -- to match gil_page
  fil_topage   INT(8)   UNSIGNED NOT NULL, -- to match page_id
  PRIMARY KEY (fil_topage, fil_wiki, fil_frompage),

  fil_dumpused0 BIGINT UNSIGNED NOT NULL DEFAULT 0, -- one bit per imported dump
  fil_dumpused1 BIGINT UNSIGNED NOT NULL DEFAULT 0

  -- fil_wiki and fil_dumpsused are logically references to wikimap and
  -- dumps respectively, but we don't tell the database that because we
  -- don't want the space overhead of creating indexes for them.  Also
  -- I think Aria doesn't support foreign keys.

  -- Use the Aria storage engine and FIXED row format for (I hope) best
  -- space efficiency.
) ENGINE = Aria,
  ROW_FORMAT = FIXED;
